<?php

// Plugin definition
$plugin = array(
  'title' => t('3 Columns Stacked: 25 - 25 - 50'),
  'category' => t('3 Columns'),
  'icon' => '3_columns_stacked_25_25_50.png',
  'theme' => 'minima_page_layout',
  'regions' => array(
    'top' => t('Top'),
    'primary' => t('Primary'),
    'secondary' => t('Secondary'),
    'tertiary' => t('Tertiary'),
    'bottom' => t('Bottom'),
  ),
);

/**
 * Implements minima_layout_LAYOUT_NAME().
 */
function minima_layout_3_columns_stacked_25_25_50($variables) {
  return array(
    'top' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--top'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--space'),
      ),
      'top' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['top'],
      ),
    ),
    'main' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--main'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--space'),
      ),
      'primary' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-xl-size2of4', 'u-xl-push2of4')
        ),
        '#markup' => $variables['content']['primary'],
      ),
      'secondary' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-xl-size1of4', 'u-xl-pull2of4')
        ),
        '#markup' => $variables['content']['secondary'],
      ),
      'tertiary' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-xl-size1of4', 'u-xl-pull2of4')
        ),
        '#markup' => $variables['content']['tertiary'],
      ),
    ),
    'bottom' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--bottom'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--space'),
      ),
      'bottom' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['bottom'],
      ),
    ),
  );
}
