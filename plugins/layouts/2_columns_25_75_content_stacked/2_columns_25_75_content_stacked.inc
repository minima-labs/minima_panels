<?php

// Plugin definition
$plugin = array(
  'title' => t('2 Columns: 25 - 75, content stacked'),
  'category' => t('2 Columns'),
  'icon' => '2_columns_25_75_content_stacked.png',
  'theme' => 'minima_page_layout',
  'regions' => array(
    'content_top' => t('Content top'),
    'content' => t('Content'),
    'content_bottom' => t('Content bottom'),
    'secondary' => t('Secondary'),
  ),
);

/**
 * Implements minima_layout_LAYOUT_NAME().
 */
function minima_layout_2_columns_25_75_content_stacked($variables) {
  return array(
    'main' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--main'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--space'),
      ),
      'primary' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-xl-size3of4', 'u-xl-push1of4')
        ),
        'content_top' => array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('content-top')
          ),
          '#markup' => $variables['content']['content_top'],
        ),
        'content' => array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('content')
          ),
          '#markup' => $variables['content']['content'],
        ),
        'content_bottom' => array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('content-bottom')
          ),
          '#markup' => $variables['content']['content_bottom'],
        ),
      ),
      'secondary' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-xl-size1of4', 'u-xl-pull3of4')
        ),
        '#markup' => $variables['content']['secondary'],
      ),
    ),
  );
}
